# <img src="icon.png" width="32" /> 1List

1List is a simple app to manage several lists from only one screen.

## Download :
<a href="https://play.google.com/store/apps/details?id=com.lolo.io.onelist"> <img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" width="165" height="64" /></a>

## Target platforms :

API 16 or later

## Features :

-   User friendly lists management and smooth navigation
-   Add, delete, edit items in your lists
-   Move items within your lists
-   Mark items as done or undone
-   Add comments on each items
-   Create an unlimited number of lists
-   Move, edit, remove your lists
-   ... more to come in the near future

## Screenshots :

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/screen1.png" width="200" /> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/screen2.png" width="200" /> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/screen3.png" width="200" /> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/screen4.png" width="200" /> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/screen5.png" width="200" /> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/screen6.png" width="200" />
